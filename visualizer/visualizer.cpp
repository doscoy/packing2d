#include <iostream>
#include <packing2d.hpp>
#include <random>
#include <string_view>

template <class T>
std::ostream& PrintXY(const std::string_view& name, T x, T y) {
  std::cout << "\"" << name << "\":{";
  std::cout << "\"x\":" << x << ",\"y\":" << y;
  std::cout << "}";
  return std::cout;
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv) {
  using T = int;
  constexpr auto kPadding{4};
  constexpr auto kMin{2};
  constexpr auto kMax{24};
  constexpr auto kLen{150};

  std::pair<T, T> size_list[kLen];
  {
    std::random_device seed_gen;
    std::default_random_engine engine(seed_gen());
    std::uniform_int_distribution<> dist(kMin, kMax);
    for (auto& size : size_list) size = {dist(engine), dist(engine)};
  }

  using Packing2D = pack::Packing2D<T>;
  constexpr std::pair<Packing2D::RESIZEMODE, std::string_view> kModeList[]{
      {Packing2D::RESIZEMODE::SQUARE, "square"},
      {Packing2D::RESIZEMODE::VERTICAL, "vertical"},
      {Packing2D::RESIZEMODE::HORIZONTAL, "horizontal"},
      {Packing2D::RESIZEMODE::POW2, "pow2"},
      {Packing2D::RESIZEMODE::SQUARE, "fix"},
      {Packing2D::RESIZEMODE::SQUARE, "fix_flip"}};

  bool first0{true};
  std::cout << "[";
  for (const auto& [mode, name] : kModeList) {
    if (not first0) std::cout << ",";
    std::cout << "{";

    std::pair<T, T> init{256, 256};
    if (mode == Packing2D::RESIZEMODE::SQUARE ||
        mode == Packing2D::RESIZEMODE::POW2) {
      init.first *= 0;
      init.second *= 0;
    }
    if (mode == Packing2D::RESIZEMODE::VERTICAL) init.second = 0;
    if (mode == Packing2D::RESIZEMODE::HORIZONTAL) init.first = 0;

    Packing2D pack{init.first, init.second, mode};
    if ((name == "fix") || (name == "fix_flip")) {
      pack = Packing2D{256, 256};
    }

    std::cout << "\"name\":\"" << name << "\",";
    PrintXY("padding", kPadding, kPadding) << ",";

    bool first1{true};
    std::cout << "\"datas\":[";
    for (auto& size : size_list) {
      const auto coord{pack.Add(size.first, size.second, kPadding, kPadding)};
      if (not coord.first) break;
      if (name == "fix_flip") pack.FlipPackingDirection();

      const auto capacity{pack.GetCapacity()};
      if (not first1) std::cout << ",";
      std::cout << "{";
      PrintXY("coords", coord.second.x, coord.second.y) << ",";
      PrintXY("size", size.first, size.second) << ",";
      PrintXY("capacity", capacity.x, capacity.y);
      std::cout << "}";

      first1 = false;
    }
    std::cout << "],";

    const auto capacity{pack.GetCapacity()};
    PrintXY("size", capacity.x, capacity.y) << ",";
    std::cout << "\"stored_count\":" << pack.GetStoreCount() << ",";
    std::cout << "\"node_count\":" << pack.GetNodeCount() << ",";
    std::cout << "\"fill_rate\":" << pack.GetFillRate() * 100;

    std::cout << "}";
    first0 = false;
  }
  std::cout << "]";
}