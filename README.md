# packing2d

矩形内に矩形を詰め込む  
[アルゴリズムの参考にしたサイト](https://blackpawn.com/texts/lightmaps/default.html)

## 使い方

```cpp
constexpr struct { int width, height; } boxes[]{...};

// 入れ物となる矩形のサイズを指定してインスタンス生成
pack::Packing2D pack{256, 256};

for (const auto& box: boxes) {
  // インスタンスに詰め込みたい矩形の幅と高さを指定して追加する
  const auto coords = pack.Add(box.width, box.height);

  // 追加に成功した場合、戻り値のfirstにtureが格納される
  if (coords.first) {
    // 戻り値のsecondに矩形の座標が格納される
    std::cout << "add [x, y] => " << coords.second.x << ", " << coords.second.y;

  } else {
    // 入れ物に空きが無い場合、メモリが確保できなかった場合に失敗となる
    std::cout << "failed to add";
    break;
  }
}
```
![fix](./img/fix.gif)

---

```cpp
constexpr struct { int width, height; } boxes[]{...};
constexpr struct { int x, y; } padding{4, 4};

// サイズを指定しない場合、入れ物は自動的に拡張される
pack::Packing2D pack;

for (const auto& box: boxes) {
  // 余白を指定して矩形を追加する
  const auto coords = pack.Add(box.width, box.height, padding.x, padding.y);

  if (coords.first) {
    std::cout << "add [x, y] => " << coords.second.x << ", " << coords.second.y;

  } else {
    // メモリが確保できなかった場合に失敗となる
    std::cout << "failed to add";
    break;
  }
}

// 入れ物の現在のサイズを取得
const auto capacity{pack.GetCapacity()};
std::cout << "capacity => " << capacity.x << ", " << capacity.y;
```
![square](./img/square_p.gif)

---

```cpp
// 入れ物の拡張方法を指定する
pack::Packing2D pack1{pack::Packing2D::RESIZEMODE::SQUARE};

// 入れ物の初期サイズと拡張方法を指定する
pack::Packing2D pack2{256, 256, pack::Packing2D::RESIZEMODE::SQUARE};
```

## リサイズモード

```cpp
// 入れ物がなるべく正方形に近づくように拡張する（デフォルト）
pack::Packing2D pack{pack::Packing2D::RESIZEMODE::SQUARE};
```
![square](./img/square_p.gif)

---

```cpp
// 水平方向に拡張する
pack::Packing2D pack{0, 256, pack::Packing2D::RESIZEMODE::HORIZONTAL};
```
![horizontal](./img/horizontal_p.gif)

---

```cpp
// 垂直方向に拡張する
pack::Packing2D pack{256, 0, pack::Packing2D::RESIZEMODE::VERTICAL};
```
![vertical](./img/vertical_p.gif)

---

```cpp
// 入れ物の辺の長さが2の冪になるように拡張する
pack::Packing2D pack{pack::Packing2D::RESIZEMODE::POW2};
```
![pow2](./img/pow2_p.gif)