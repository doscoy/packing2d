#pragma once

#include <algorithm>
#include <cmath>
#include <memory>
#include <tuple>

namespace pack {

template <class T = int>
class Packing2D {
  class Node;

 public:
  struct Vec2 {
    T x, y;
  };

  enum class RESIZEMODE { SQUARE, VERTICAL, HORIZONTAL, POW2 };

  static constexpr std::pair<bool, Vec2> kInvalid{
      false, {static_cast<T>(0), static_cast<T>(0)}};

  Packing2D(T capacity_x, T capacity_y, RESIZEMODE mode) noexcept;
  Packing2D(T capacity_x, T capacity_y) noexcept;
  Packing2D(RESIZEMODE mode) noexcept;
  Packing2D() noexcept;

  Packing2D(const Packing2D&) = delete;
  Packing2D& operator=(const Packing2D&) = delete;

  Packing2D(Packing2D&&) noexcept = default;
  Packing2D& operator=(Packing2D&&) noexcept = default;

  auto GetCapacity() const noexcept -> const Vec2&;
  auto GetNodeCount() const noexcept -> std::size_t;
  auto GetStoreCount() const noexcept -> std::size_t;
  auto GetStoreArea() const noexcept -> std::size_t;
  auto GetFillRate() const noexcept -> double;
  auto GetResizeMode() const noexcept -> RESIZEMODE;

  auto IsFixedCapacity() const noexcept -> bool;
  auto IsPackingForward() const noexcept -> bool;

  void FlipPackingDirection() noexcept;

  auto Add(T x, T y) noexcept -> std::pair<bool, Vec2>;
  auto Add(T x, T y, T padding_x, T padding_y) noexcept
      -> std::pair<bool, Vec2>;

 private:
  static auto PowCheck(T n) noexcept -> bool;
  static auto PowFix(T n) noexcept -> T;
  static auto PowFix(const Vec2& size) noexcept -> Vec2;

  void Init(const Vec2& added) noexcept;

  auto IncreaseCapacityToSquare(const Vec2& added) noexcept
      -> std::pair<bool, Vec2>;
  auto IncreaseCapacityToVertical(const Vec2& added) noexcept
      -> std::pair<bool, Vec2>;
  auto IncreaseCapacityToHorizontal(const Vec2& added) noexcept
      -> std::pair<bool, Vec2>;
  auto IncreaseCapacityToPow2(const Vec2& added) noexcept
      -> std::pair<bool, Vec2>;
  auto CalcNewCapacity(const Vec2& added) noexcept -> std::pair<bool, Vec2>;

  auto Resize(const Vec2& added) noexcept -> bool;

  Node root_;
  RESIZEMODE resize_mode_;
  bool fixed_capacity_;
  bool packing_forward_{true};
  std::size_t stored_counter_{0};
  T stored_area_{0};
};

template <class T>
class Packing2D<T>::Node {
  struct Rect {
    Vec2 coord, size;
  };
  using NodePtr = std::unique_ptr<Node>;
  using Bros = std::pair<NodePtr, NodePtr>;

 public:
  enum class STATE { NO_STATE, FIND, ALREADY_USED, TOO_SMALL, BAD_ALLOC };

  Node(const Vec2& size) noexcept;
  Node() noexcept;

  auto GetCount() const noexcept -> std::size_t;
  auto GetCoord() const noexcept -> const Vec2&;
  auto GetSize() const noexcept -> const Vec2&;

  void Reset(const Vec2& size) noexcept;
  void Replace(const Node* const target, NodePtr* source) noexcept;
  auto Add(const Vec2& size, const bool packing_forward) noexcept
      -> std::tuple<STATE, Node*, Node*>;

 private:
  Node(const Rect& rect) noexcept;

  auto Add(Node* const elder, const Vec2& size,
           const bool packing_forward) noexcept
      -> std::tuple<STATE, Node*, Node*>;
  auto NewBros(const Vec2& size) noexcept -> Bros;
  auto NewBrosReverse(const Vec2& size) noexcept -> Bros;

  Rect rect_;
  Bros bros_;
  bool use_{false};
};

template <class T>
Packing2D<T>::Packing2D(T capacity_x, T capacity_y, RESIZEMODE mode) noexcept
    : root_(Vec2{capacity_x, capacity_y}),
      resize_mode_(mode),
      fixed_capacity_(false) {}

template <class T>
Packing2D<T>::Packing2D(T capacity_x, T capacity_y) noexcept
    : root_(Vec2{capacity_x, capacity_y}),
      resize_mode_(RESIZEMODE::SQUARE),
      fixed_capacity_(true) {}

template <class T>
Packing2D<T>::Packing2D(RESIZEMODE mode) noexcept
    : root_(), resize_mode_(mode), fixed_capacity_(false) {}

template <class T>
Packing2D<T>::Packing2D() noexcept
    : root_(), resize_mode_(RESIZEMODE::SQUARE), fixed_capacity_(false) {}

template <class T>
inline auto Packing2D<T>::GetCapacity() const noexcept -> const Vec2& {
  return root_.GetSize();
}

template <class T>
inline auto Packing2D<T>::GetNodeCount() const noexcept -> std::size_t {
  return root_.GetCount();
}

template <class T>
inline auto Packing2D<T>::GetStoreCount() const noexcept -> std::size_t {
  return stored_counter_;
}

template <class T>
inline auto Packing2D<T>::GetStoreArea() const noexcept -> std::size_t {
  return stored_area_;
}

template <class T>
inline auto Packing2D<T>::GetFillRate() const noexcept -> double {
  const auto capacity{GetCapacity()};
  return static_cast<double>(stored_area_) /
         static_cast<double>(capacity.x * capacity.y);
}

template <class T>
inline auto Packing2D<T>::GetResizeMode() const noexcept -> RESIZEMODE {
  return resize_mode_;
}

template <class T>
inline auto Packing2D<T>::IsFixedCapacity() const noexcept -> bool {
  return fixed_capacity_;
}

template <class T>
inline auto Packing2D<T>::IsPackingForward() const noexcept -> bool {
  return packing_forward_;
}

template <class T>
inline void Packing2D<T>::FlipPackingDirection() noexcept {
  packing_forward_ = !packing_forward_;
}

template <class T>
auto Packing2D<T>::Add(T x, T y) noexcept -> std::pair<bool, Vec2> {
  const Vec2 added{x, y};
  auto [state, elder, node]{root_.Add(added, IsPackingForward())};
  if (state == Node::STATE::FIND) {
    ++stored_counter_;
    stored_area_ += added.x * added.y;
    return {true, node->GetCoord()};

  } else if ((state == Node::STATE::BAD_ALLOC) || fixed_capacity_) {
    return kInvalid;

  } else if (state == Node::STATE::TOO_SMALL) {
    const bool is_root{elder == nullptr};
    if (is_root) {
      Init(added);
      return Add(added.x, added.y);

    } else {
      return Resize(added) ? Add(added.x, added.y) : kInvalid;
    }

  } else if (state == Node::STATE::ALREADY_USED) {
    return Resize(added) ? Add(added.x, added.y) : kInvalid;

  } else {
    return kInvalid;
  }
}

template <class T>
auto Packing2D<T>::Add(T x, T y, T padding_x, T padding_y) noexcept
    -> std::pair<bool, Vec2> {
  const Vec2 with_padding{x + padding_x * 2, y + padding_y * 2};
  const auto result{Add(with_padding.x, with_padding.y)};
  if (not result.first) {
    return kInvalid;

  } else {
    return {true, {result.second.x + padding_x, result.second.y + padding_y}};
  }
}

template <class T>
inline auto Packing2D<T>::PowCheck(T n) noexcept -> bool {
  double i{0.0};
  return std::modf(std::log2(n), &i) == 0.0;
}

template <class T>
inline auto Packing2D<T>::PowFix(T n) noexcept -> T {
  const int lg{static_cast<int>(std::log2(n))};
  return static_cast<T>(std::pow(2, lg + 1));
}

template <class T>
inline auto Packing2D<T>::PowFix(const Vec2& vec2) noexcept -> Vec2 {
  return Vec2{PowFix(vec2.x), PowFix(vec2.y)};
}

template <class T>
void Packing2D<T>::Init(const Vec2& added) noexcept {
  const auto capacity{GetCapacity()};
  const Vec2 reset{std::max(added.x, capacity.x),
                   std::max(added.y, capacity.y)};
  if (resize_mode_ == RESIZEMODE::POW2) {
    root_.Reset(PowFix(reset));

  } else {
    root_.Reset(reset);
  }
}

template <class T>
auto Packing2D<T>::IncreaseCapacityToSquare(const Vec2& added) noexcept
    -> std::pair<bool, Vec2> {
  const auto current_capacity{root_.GetSize()};
  const Vec2 new_capacity{current_capacity.x + added.x,
                          current_capacity.y + added.y};
  if (new_capacity.x < new_capacity.y) {
    return {true,
            {std::max(new_capacity.x, current_capacity.x), current_capacity.y}};

  } else {
    return {true,
            {current_capacity.x, std::max(new_capacity.y, current_capacity.y)}};
  }
}

template <class T>
auto Packing2D<T>::IncreaseCapacityToVertical(const Vec2& added) noexcept
    -> std::pair<bool, Vec2> {
  const auto current_capacity{root_.GetSize()};
  if (added.x > current_capacity.x) {
    return kInvalid;

  } else {
    return {true, {current_capacity.x, current_capacity.y + added.y}};
  }
}

template <class T>
auto Packing2D<T>::IncreaseCapacityToHorizontal(const Vec2& added) noexcept
    -> std::pair<bool, Vec2> {
  const auto current_capacity{root_.GetSize()};
  if (added.y > current_capacity.y) {
    return kInvalid;

  } else {
    return {true, {current_capacity.x + added.x, current_capacity.y}};
  }
}

template <class T>
auto Packing2D<T>::IncreaseCapacityToPow2(const Vec2& added) noexcept
    -> std::pair<bool, Vec2> {
  const auto current_capacity{root_.GetSize()};
  const Vec2 new_capacity{current_capacity.x + added.x,
                          current_capacity.y + added.y};
  const auto pow_new_capacity{PowFix(new_capacity)};
  const Vec2 delta{pow_new_capacity.x - current_capacity.x,
                   pow_new_capacity.y - current_capacity.y};
  const bool add_x{delta.x != delta.y ? delta.x > delta.y
                                      : new_capacity.x < new_capacity.y};
  const auto fix_func{[](const T n) { return PowCheck(n) ? n : PowFix(n); }};
  if (add_x) {
    const T fix_x{fix_func(current_capacity.x)};
    const T x{fix_x < added.x ? pow_new_capacity.x : fix_x};
    return {true, {x, pow_new_capacity.y}};

  } else {
    const T fix_y{fix_func(current_capacity.y)};
    const T y{fix_y < added.y ? pow_new_capacity.y : fix_y};
    return {true, {pow_new_capacity.x, y}};
  }
}

template <class T>
auto Packing2D<T>::CalcNewCapacity(const Vec2& added) noexcept
    -> std::pair<bool, Vec2> {
  switch (resize_mode_) {
    case RESIZEMODE::SQUARE:
      return IncreaseCapacityToSquare(added);
    case RESIZEMODE::VERTICAL:
      return IncreaseCapacityToVertical(added);
    case RESIZEMODE::HORIZONTAL:
      return IncreaseCapacityToHorizontal(added);
    case RESIZEMODE::POW2:
      return IncreaseCapacityToPow2(added);
    default:
      return IncreaseCapacityToSquare(added);
  }
}

template <class T>
auto Packing2D<T>::Resize(const Vec2& added) noexcept -> bool {
  // need to call "CalcNewCapacity" it out before move the root_
  const auto new_capacity{CalcNewCapacity(added)};
  if (not new_capacity.first) return false;

  std::unique_ptr<Node> root_copy{new Node{std::move(root_)}};
  const auto restoration{[this, &root_copy] {
    root_ = std::move(*root_copy);
    return false;
  }};
  root_.Reset(new_capacity.second);

  auto [state, elder, node]{root_.Add(root_copy->GetSize(), true)};
  if (state != Node::STATE::FIND) {
    return restoration();
  }

  elder->Replace(node, &root_copy);
  if (root_copy) {
    return restoration();
  }

  return true;
}

template <class T>
Packing2D<T>::Node::Node(const Rect& rect) noexcept
    : rect_(rect), bros_{nullptr, nullptr} {}

template <class T>
Packing2D<T>::Node::Node(const Vec2& size) noexcept
    : Node{Rect{{0, 0}, size}} {}

template <class T>
Packing2D<T>::Node::Node() noexcept : Node{Vec2{0, 0}} {}

template <class T>
auto Packing2D<T>::Node::GetCount() const noexcept -> std::size_t {
  if (bros_.first && bros_.second) {
    return bros_.first->GetCount() + bros_.second->GetCount();

  } else {
    return 1;
  }
}

template <class T>
inline auto Packing2D<T>::Node::GetCoord() const noexcept -> const Vec2& {
  return rect_.coord;
}

template <class T>
inline auto Packing2D<T>::Node::GetSize() const noexcept -> const Vec2& {
  return rect_.size;
}

template <class T>
inline void Packing2D<T>::Node::Reset(const Vec2& size) noexcept {
  rect_.size = size;
  bros_.first.reset(nullptr);
  bros_.second.reset(nullptr);
  use_ = false;
}

template <class T>
inline void Packing2D<T>::Node::Replace(const Node* const target,
                                        NodePtr* source) noexcept {
  if (bros_.first.get() == target) {
    bros_.first = std::move(*source);
    source->reset(nullptr);

  } else if (bros_.second.get() == target) {
    bros_.second = std::move(*source);
    source->reset(nullptr);
  }
}

template <class T>
auto Packing2D<T>::Node::Add(const Vec2& size,
                             const bool packing_forward) noexcept
    -> std::tuple<STATE, Node*, Node*> {
  return Add(nullptr, size, packing_forward);
}

template <class T>
auto Packing2D<T>::Node::Add(Node* const elder, const Vec2& size,
                             const bool packing_forward) noexcept
    -> std::tuple<STATE, Node*, Node*> {
  if (bros_.first && bros_.second) {
    auto tmp{bros_.first->Add(this, size, packing_forward)};
    if (std::get<0>(tmp) == STATE::FIND) {
      return tmp;

    } else {
      return bros_.second->Add(this, size, packing_forward);
    }
  } else if (use_) {
    return {STATE::ALREADY_USED, elder, this};

  } else if ((rect_.size.x < size.x) || (rect_.size.y < size.y)) {
    return {STATE::TOO_SMALL, elder, this};

  } else if ((rect_.size.x == size.x) && (rect_.size.y == size.y)) {
    use_ = true;
    return {STATE::FIND, elder, this};

  } else {
    this->bros_ = packing_forward ? NewBros(size) : NewBrosReverse(size);

    if (this->bros_.first && this->bros_.second) {
      return this->bros_.first->Add(this, size, packing_forward);

    } else {
      return {STATE::BAD_ALLOC, elder, this};
    }
  }
}

template <class T>
auto Packing2D<T>::Node::NewBros(const Vec2& size) noexcept -> Bros {
  Bros baby{new (std::nothrow) Node{rect_}, new (std::nothrow) Node{rect_}};

  if (baby.first && baby.second) {
    const Vec2 delta{rect_.size.x - size.x, rect_.size.y - size.y};
    if (delta.x > delta.y) {
      baby.first->rect_.size.x = size.x;
      baby.second->rect_.coord.x += size.x;
      baby.second->rect_.size.x -= size.x;

    } else {
      baby.first->rect_.size.y = size.y;
      baby.second->rect_.coord.y += size.y;
      baby.second->rect_.size.y -= size.y;
    }
  }

  return baby;
}

template <class T>
auto Packing2D<T>::Node::NewBrosReverse(const Vec2& size) noexcept -> Bros {
  Bros baby{new (std::nothrow) Node{rect_}, new (std::nothrow) Node{rect_}};

  if (baby.first && baby.second) {
    const Vec2 delta{rect_.size.x - size.x, rect_.size.y - size.y};
    if (delta.x > delta.y) {
      baby.first->rect_.coord.x += rect_.size.x - size.x;
      baby.first->rect_.size.x = size.x;
      baby.second->rect_.size.x -= size.x;

    } else {
      baby.first->rect_.coord.y += rect_.size.y - size.y;
      baby.first->rect_.size.y = size.y;
      baby.second->rect_.size.y -= size.y;
    }
  }

  return baby;
}

}  // namespace pack